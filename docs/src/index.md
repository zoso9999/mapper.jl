# Reference
This page has references to all exported types and functions.

## Types
```@docs
DefaultDict
```

## Functions
```@docs
simplifyCells
circle
cirlceOpt
helix
disk
helicoid
ring
cylinder
sphere
toroidal
toroidalOpt
crown
cuboid
cuboidOpt
ball
rod
hollowCyl
hollowBall
torus
pizza
```